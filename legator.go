package legate

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"os/exec"
)

const LEGATE_PORT_HEADER = "X-LEGATE-PORT"

type Legator struct {
	listeningPort  string
	forwardingPort string
}

func (l *Legator) NewRule(path string, ruleFunc func(*http.Request)) {

}

func (l *Legator) IsInternal(*http.Request) bool {
	return true
}

func execCmd(cmdArgs []string) {
	cmd := exec.Command(cmdArgs[0], cmdArgs[1:]...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}

func setProxy(l *Legator) *httputil.ReverseProxy {
	director := func(req *http.Request) {
		// req = r
		url, _ := url.Parse("http://localhost:" + l.forwardingPort)
		req.URL = url
		req.Header.Del(LEGATE_PORT_HEADER)
		req.Header.Add(LEGATE_PORT_HEADER, l.listeningPort)
		req.URL.Path = "http://localhost:3000/test"
		req.Header.Add("LEGATE_TEST", "Hello world!")
		req.Method = "POST"
	}

	proxy := &httputil.ReverseProxy{Director: director}
	return proxy
}

func (l *Legator) Run(args ...string) {

	ports := os.Args[1:3]
	cmdArgs := os.Args[3:]

	l.listeningPort = ports[0]  // listening port
	l.forwardingPort = ports[1] // forwarding port

	// Check if we ask legate to run standalone
	// Register with mesh using mesh key and service name, if not in args use ENV vars
	// loads mesh config (master mesh host) from ENV vars
	if !*Standalone {
		registerServiceInstance()
		defer unregisterServiceInstance()
	}

	// exec underlying process
	go execCmd(cmdArgs)

	// start http proxy
	err := http.ListenAndServe(":"+l.listeningPort, setProxy(l))

	fmt.Println("err")
	log.Fatal(err)
}
