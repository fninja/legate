package legate

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

func listenSignalInterupt() {
	var stopChan = make(chan os.Signal, 2)
	signal.Notify(stopChan, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)

	<-stopChan
	unregisterServiceInstance()
}

func registerServiceInstance() {
	go listenSignalInterupt()
}

func unregisterServiceInstance() {
	fmt.Println("unregistering")
	fmt.Println(ServiceName)
	fmt.Println(LegateKey)
}
