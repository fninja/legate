package main

import (
	"net/http"
	"os"

	"bitbucket.org/fninja/legate"
)

func main() {
	legator := legate.New()

	legator.NewRule(
		"*",
		func(req *http.Request) {
			req.URL.Path = "http://localhost:3000/test"
			req.Header.Add("LEGATE_TEST", "Hello world!")
			req.Method = "POST"
			if legator.IsInternal(req) {
				return
			}
			return
		},
	)

	legator.NewRule(
		"*",
		func(req *http.Request) {
			req.URL.Path = "http://localhost:3000/test"
			req.Header.Add("LEGATE_TEST", "Hello world!")
			if legator.IsInternal(req) {
				return
			}
			return
		},
	)

	legator.Run(os.Getenv("LEGATE_KEY"), os.Getenv("LEGATE_SERVICE"))
}
