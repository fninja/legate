package legate

import "flag"

var ServiceName = flag.String("service", "", "Service name")
var LegateKey = flag.String("key", "", "Legate key")
var Standalone = flag.Bool("standalone", false, "Should legate run in a standalone mode")

func New() *Legator {
	return &Legator{}
}
